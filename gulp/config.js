'use strict';

var concat = require('gulp-concat');

var src = 'src';
var dest = 'dist';

module.exports = {
  serverport: 4000,
  src: src,
  dest: dest,

  sass: {
      includePaths: [
        'node_modules/bootstrap-sass/assets/stylesheets/'
      ]
  },

  watch: {
    paths: ['js', 'scss', 'html'].reduce(function(paths, ext) {
      return paths.concat([
        src + '/**/*.' + ext,
        src + '/*.' + ext,
        '!' + src + '/**/*_test.' + ext,
        '!' + src + '/*_test.' + ext
      ]);
    }, [])
  },

  output: {
    filename: 'main.js'
  },

  inject: {
    addRootSlash: true,
    relative: true
  }
};
