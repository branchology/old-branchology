'use strict';

var config = require('../config'),
  gulp = require('gulp'),
  inject = require('gulp-inject');

gulp.task('inject', ['clean', 'copy', 'browserify', 'sass'], function() {
  var sources = gulp.src([
    config.dest + '/*.js',
    config.dest + '/*.css'
  ], {read: false});
  return gulp.src(config.dest + '/*.html')
     .pipe(inject(sources, config.inject))
     .pipe(gulp.dest(config.dest));
});
