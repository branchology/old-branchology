'use strict';

var browserSync = require('browser-sync'),
  config = require('../config'),
  connectBrowserSync = require('connect-browser-sync'),
  express = require('express'),
  gulp = require('gulp'),
  gutil = require('gulp-util'),
  http = require('http'),
  morgan = require('morgan');

gulp.task('reload-browser', ['build', 'inject'], function() {
  browserSync.reload();
});

gulp.task('server', ['build', 'watch'], function() {
  var app = createApp();
  var server = createServer(app);

  server.listen(config.serverport, function () {
    gutil.log('Development server started at http://localhost:' + config.serverport);
  });
});

function createApp() {
  var app = express(),
    bs = browserSync({logSnippet: false});

  app.use(connectBrowserSync(bs));
  app.use(morgan('dev'));
  app.use(express.static(config.dest));

  app.all('/*', function(req, res) {
    res.sendFile('index.html', {root: config.dest});
  });

  return app;
}

function createServer(app) {
  var server = http.createServer(app);

  server.on('error', function(err) {
    if (err.code === 'EADDRINUSE'){
      gutil.log('Development server is already started at port ' + config.serverport);
    } else {
      throw err;
    }
  });

  return server;
}
