'use strict';

var config = require('../config'),
  gulp = require('gulp');

gulp.task('watch', ['build'], function() {
  return gulp.watch(
    config.watch.paths,
    ['build', 'reload-browser']
  );
});
