'use strict';

var concat = require('gulp-concat'),
  config = require('../config'),
  gulp = require('gulp'),
  gutil = require('gulp-util'),
  minify = require('gulp-minify-css'),
  sass = require('gulp-sass');

var output = 'main.css';

if (gutil.env.production || gutil.env.staging) {
  var rev = Math.random().toString(36).substring(3);
  output = 'main' + rev + '.css';
}

gulp.task('sass', ['clean'], function() {
  return gulp.src(config.src + '/main.scss')
    .pipe(sass(config.sass))
    .pipe(concat(output))
    .pipe(gutil.env.production ? minify({keepBreaks:true}) : gutil.noop())
    .pipe(gulp.dest(config.dest));
});
