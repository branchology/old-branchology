'use strict';

var config = require('../config'),
  del = require('del'),
  gulp = require('gulp');

gulp.task('clean', function (cb) {
  del([config.dest], cb);
});
