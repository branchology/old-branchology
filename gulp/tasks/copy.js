'use strict';

var config = require('../config'),
  gulp = require('gulp');

gulp.task('copy-fonts', ['clean'], function () {
  return gulp.src([
    config.src + '/components/fonts/*.{ttf,woff,svg,eot}',
  ]).pipe(gulp.dest(config.dest + '/fonts'));
});

gulp.task('copy-images', ['clean'], function () {
  return gulp.src(config.src + '/img/*.{png,gif,jpg}')
    .pipe(gulp.dest(config.dest + '/img'));
});

gulp.task('copy-html', ['clean'], function () {
  return gulp.src([
    config.src + '/*.html',
    config.src + '/**/*.html'
  ]).pipe(gulp.dest(config.dest));
});

gulp.task('copy', ['copy-fonts', 'copy-images', 'copy-html']);
