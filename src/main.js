'use strict';

require('angular');
require('angular-cookies');
require('angular-resource');
require('angular-bootstrap');
require('ui-router');
require('angular-hal-collection/src/collection');

require('./app');
require('./config');

require('./components/services/auth/auth');

require('./components/resources/resources');
require('./components/resources/person');
require('./components/resources/source');

require('./components/collections/collections');
require('./components/collections/person-collection');
require('./components/collections/source-collection');

require('./login/login');
require('./login/login-controller');

require('./navbar/navbar');
require('./navbar/navbar-controller');

require('./dashboard/dashboard');
require('./dashboard/dashboard-controller');

require('./people/people');
require('./people/people-controller');
require('./people/view-person-controller');

require('./sources/sources');
require('./sources/sources-controller');
require('./sources/view-source-controller');
