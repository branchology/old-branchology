describe('LoginController', function() {
  beforeEach(angular.mock.module('branchology.login'));

  beforeEach(inject(function($location, $controller, Auth) {
    this.$location = $location;
    this.Auth = Auth;
    this.controller = $controller('LoginController', {$location: this.$location, Auth: this.Auth});
  }));

  describe('processLogin()', function() {
    it('should process the login', inject(function($q, $rootScope) {
      spyOn(this.Auth, 'authenticate').and.returnValue($q.when());

      this.controller.email = 'bob';
      this.controller.password = 'pass';
      this.controller.processLogin();
      $rootScope.$digest();

      expect(this.Auth.authenticate).toHaveBeenCalledWith('bob', 'pass');
    }));

    it('should redirect to the /dashboard on success', inject(function($q, $rootScope) {
      spyOn(this.Auth, 'authenticate').and.returnValue($q.when());
      spyOn(this.$location, 'path');

      this.controller.processLogin();
      $rootScope.$digest();

      expect(this.$location.path).toHaveBeenCalledWith('/dashboard');
    }));

    it('should do something useful on failure');
  });
});
