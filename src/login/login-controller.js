(function() {
  'use strict';

  LoginController.$inject = ['$location', 'Auth'];

  function LoginController($location, Auth) {
    this.$location = $location;
    this.Auth = Auth;

    this.email = '';
    this.password = '';
  }

  LoginController.prototype.processLogin = function() {
    var that = this;

    this.Auth.authenticate(this.email, this.password).then(
      function() {
        that.$location.path('/dashboard');
      },
      function() {
        // probably do something to inform the user that they are not gonna be logged in today
      }
    );
  };

  angular.module('branchology.login')
    .controller('LoginController', LoginController);

})();
