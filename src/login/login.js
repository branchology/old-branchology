(function() {
  'use strict';

  moduleConfig.$inject = ['$stateProvider'];

  function moduleConfig($stateProvider) {
    $stateProvider.state('login', {
      url: '/login',
      controller: 'LoginController as vm',
      templateUrl: 'login/login.html',
    });
  }

  angular.module('branchology.login', [
    'ui.router',
    'branchology.auth'
  ])
    .config(moduleConfig);

})();
