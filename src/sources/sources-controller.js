(function() {
  'use strict';

  SourcesController.$inject = ['sources'];

  function SourcesController(sources) {
    this.sources = sources.items;
  }

  angular.module('branchology.sources')
    .controller('SourcesController', SourcesController);

})();
