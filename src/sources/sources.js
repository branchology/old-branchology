(function() {
  'use strict';

  resolveSources.$inject = ['SourceCollection'];

  function resolveSources(SourceCollection) {
    return SourceCollection.get();
  }

  resolveSource.$inject = ['$stateParams', 'Source'];

  function resolveSource($stateParams, Source) {
    return Source.get({id: $stateParams.id}).$promise;
  }

  moduleConfig.$inject = ['$stateProvider'];

  function moduleConfig($stateProvider) {
    $stateProvider.state('app.sources', {
      url: '/sources',
      controller: 'SourcesController as vm',
      templateUrl: 'sources/sources-list.html',
      activeMenu: 'sources',
      resolve: {
        sources: resolveSources
      }
    });

    $stateProvider.state('app.view-source', {
      url: '/sources/:id',
      controller: 'ViewSourceController as vm',
      templateUrl: 'sources/view-source.html',
      activeMenu: 'sources',
      resolve: {
        source: resolveSource
      }
    });
  }

  angular.module('branchology.sources', [
    'ui.router',
    'branchology.resources',
    'branchology.collections'
  ])
    .config(moduleConfig);

})();
