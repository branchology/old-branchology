(function() {
  'use strict';

  ViewSourceController.$inject = ['source'];

  function ViewSourceController(source) {
    this.source = source;
  }

  angular.module('branchology.sources')
    .controller('ViewSourceController', ViewSourceController);

})();
