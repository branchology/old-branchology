(function() {
  'use strict';

  moduleConfig.$inject = ['$stateProvider'];

  function moduleConfig($stateProvider) {
    $stateProvider.state('app.dashboard', {
      url: '/dashboard',
      controller: 'DashboardController',
      controllerAs: 'vm',
      templateUrl: 'dashboard/dashboard.html',
      activeMenu: 'dashboard'
    });
  }

  angular.module('branchology.dashboard', [
    'ui.router'
  ])
    .config(moduleConfig);

})();
