(function() {
  'use strict';

  DashboardController.$inject = [];

  function DashboardController() {
  }

  angular.module('branchology.dashboard')
    .controller('DashboardController', DashboardController);

})();
