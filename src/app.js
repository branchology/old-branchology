(function() {
  'use strict';

  config.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];

  function config($locationProvider, $stateProvider, $urlRouterProvider) {
    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise('/dashboard');

    // I don't particularly think trailing slashes are pretty
    $urlRouterProvider.rule(function($injector, $location) {
      var path = $location.path(), search = $location.search();
      if (path[path.length-1] === '/') {
        if (search === {}) {
          return path.substr(0, path.length - 1);
        } else {
          var params = [];
          angular.forEach(search, function(v, k) {
            params.push(k + '=' + v);
          });
          return path.substr(0, path.length - 1) + '?' + params.join('&');
        }
      }
  });

    $stateProvider.state('app', {
      templateUrl: 'components/layout/app.html',
    });
  }

  function run($location, Auth) {
    if (!Auth.isAuthenticated()) {
      $location.path('/login');
    }
  }

  angular.module('branchology', [
    'ui.router',
    'ui.bootstrap',
    'branchology.resources',
    'branchology.collections',
    'branchology.auth',

    'branchology.login',
    'branchology.navbar',
    'branchology.dashboard',
    'branchology.people',
    'branchology.sources'
  ])
    .config(config)
    .run(run);

})();
