(function() {
  'use strict';

  NavbarController.$inject = [];

  function NavbarController() {
    this.isCollapsed = true;
    // this.$route = $route;
  }

  NavbarController.prototype.isRouteActive = function(route) {
    return false;
    // return 'activeMenu' in this.$route.current ?
    //     this.$route.current.activeMenu == route : false;
  }

  angular.module('branchology.navbar')
    .controller('NavbarController', NavbarController);

})();
