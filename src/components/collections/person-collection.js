(function() {
  'use strict';

  PersonCollection.$inject = ['$collection', 'config', 'Person'];

  function PersonCollection($collection, config, Person) {
    return $collection(config.ApiRoot + '/people', Person, 'people');
  }

  angular.module('branchology.collections')
    .factory('PersonCollection', PersonCollection);

})();
