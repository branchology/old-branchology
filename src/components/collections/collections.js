(function() {
  'use strict';

  angular.module('branchology.collections', [
    'hal-collection',
    'branchology.config',
    'branchology.resources'
  ]);

})();
