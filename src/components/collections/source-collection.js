(function() {
  'use strict';

  SourceCollection.$inject = ['$collection', 'config', 'Source'];

  function SourceCollection($collection, config, Source) {
    return $collection(config.ApiRoot + '/sources', Source, 'sources');
  }

  angular.module('branchology.collections')
    .factory('SourceCollection', SourceCollection);

})();
