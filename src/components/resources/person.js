(function() {
  'use strict';

  PersonFactory.$inject = ['$resource', 'config'];

  function PersonFactory($resource, config) {
    var Person = $resource(config.ApiRoot + '/people/:id', {id: '@id'}, {
      update: {method: 'PATCH'}
    });

    Object.defineProperties(Person.prototype, {
      events: {
        get: function () {
          return this._embedded.events;
        }
      }
    });

    return Person;
  }

  angular.module('branchology.resources')
    .factory('Person', PersonFactory);

})();
