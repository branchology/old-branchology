(function() {
  'use strict';

  angular.module('branchology.resources', [
    'ngResource',
    'branchology.config'
  ]);

})();
