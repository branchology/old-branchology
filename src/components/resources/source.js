(function() {
  'use strict';

  SourceFactory.$inject = ['$resource', 'config'];

  function SourceFactory($resource, config) {
    return $resource(config.ApiRoot + '/sources/:id');
  }

  angular.module('branchology.resources')
    .factory('Source', SourceFactory);

})();
