(function() {
  'use strict';

  AuthenticationService.$inject = ['$http', '$cookies', 'config'];

  function AuthenticationService($http, $cookies, config) {
    this.$http = $http;
    this.$cookies = $cookies;
    this.config = config;
  }

  AuthenticationService.prototype.isAuthenticated = function() {
    return !!this.$cookies.get('access_token');
  };

  AuthenticationService.prototype.setCookies = function(token) {
    this.$cookies.put('access_token', token.token);
    this.$cookies.put('user', JSON.stringify(token._embedded.user[0]));
  };

  AuthenticationService.prototype.authenticate = function(email, password) {
    var that = this;

    return this.$http.post(
      this.config.ApiRoot + '/login',
      {email: email, password: password}
    ).then(function (response) {
      that.setCookies(response.data);
      return response.data;
    });
  };

  authInterceptor.$inject = ['$cookies', '$location', '$q'];

  function authInterceptor($cookies, $location, $q) {
    return {
      request: function(req) {
        if ($cookies.get('access_token')) {
          req.headers['Authorization'] = 'Bearer ' + $cookies.get('access_token');
        }

        return req;
      },
      responseError: function(response) {
        if (response.status && response.status == 401) {
          $location.path('/login');

          delete $cookies.access_token;
          delete $cookies.user;

          return $q.when(response);
        }

        return $q.reject(response);
      }
    };
  }

  moduleConfig.$inject = ['$httpProvider'];

  function moduleConfig($httpProvider) {
    $httpProvider.interceptors.push(authInterceptor);
  }

  angular.module('branchology.auth', [
    'ngCookies',
    'branchology.config'
  ])
    .service('Auth', AuthenticationService)
    .config(moduleConfig);

})();
