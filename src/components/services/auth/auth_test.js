describe('Auth', function() {
  beforeEach(angular.mock.module('branchology.auth'));

  beforeEach(inject(function($cookies, Auth) {
    this.$cookies = $cookies;
    this.Auth = Auth;
  }));

  describe('config()', function() {
    describe('httpInterceptor', function() {
      beforeEach(inject(function(_$httpBackend_, $http) {
        this.$httpBackend = _$httpBackend_;
        this.$http = $http;
      }));

      it('should leave the request alone when not authorized', function() {
        this.$httpBackend.expectGET('/foo', function(headers) {
          return !headers.Authorization;
        }).respond(200);
        this.$http.get('/foo');
        this.$httpBackend.flush();
      });

      it('should inject an auth header when authorized', function() {
        this.$cookies.put('access_token', '8675309');
        this.$httpBackend.expectGET('/foo', function(headers) {
          return headers.Authorization === 'Bearer 8675309';
        }).respond(200);
        this.$http.get('/foo');
        this.$httpBackend.flush();
        this.$cookies.remove('access_token');
      });
    });
  });

  describe('isAuthenticated()', function() {
    it('should return true if an access token cookie exists', function() {
      this.$cookies.put('access_token', 'yes');
      expect(this.Auth.isAuthenticated()).toBe(true);
      this.$cookies.remove('access_token');
    });

    it('should return false if an access token cookie does not exist', function() {
      expect(this.Auth.isAuthenticated()).toBe(false);
    });
  });

  describe('authenticate()', function() {
    beforeEach(inject(function(_$httpBackend_, config) {
      this.$httpBackend = _$httpBackend_;
      this.config = config;
    }));

    it('should make a POST to /login', function() {
      var response = {token: 'my-token'};

      this.$httpBackend
        .expectPOST(this.config.ApiRoot + '/login')
        .respond(500);
      this.Auth.authenticate('bob', 'pass');
      this.$httpBackend.flush();
    });

    it('should set the cookies on a successful login', function() {
      var response = {token: 'my-token'};

      this.$httpBackend
        .expectPOST(this.config.ApiRoot + '/login')
        .respond(200, JSON.stringify(response));

      spyOn(this.Auth, 'setCookies');

      this.Auth.authenticate('bob', 'pass');
      this.$httpBackend.flush();

      expect(this.Auth.setCookies).toHaveBeenCalledWith(response);
    });
  });
});
