(function() {
  'use strict';

  var appConfig = {
    ApiRoot: 'http://api.branchology.dev'
  };

  angular.module('branchology.config', [])
    .constant('config', appConfig);
    
})();
