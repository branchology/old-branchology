(function() {
  'use strict';

  ViewPersonController.$inject = ['person'];

  function ViewPersonController(person) {
    this.person = person;
  }

  angular.module('branchology.people')
    .controller('ViewPersonController', ViewPersonController);

})();
