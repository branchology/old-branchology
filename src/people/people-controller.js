(function() {
  'use strict';

  PeopleController.$inject = ['people'];

  function PeopleController(people) {
    this.people = people.items;
  }

  angular.module('branchology.people')
    .controller('PeopleController', PeopleController);

})();
