(function() {
  'use strict';

  resolvePeople.$inject = ['PersonCollection'];

  function resolvePeople(PersonCollection) {
    return PersonCollection.get({sort: 'person.surname', order: 'asc'});
  }

  resolvePerson.$inject = ['$stateParams', 'Person'];

  function resolvePerson($stateParams, Person) {
    return Person.get({id: $stateParams.id}).$promise;
  }

  moduleConfig.$inject = ['$stateProvider'];

  function moduleConfig($stateProvider) {
    $stateProvider.state('app.people', {
      url: '/people',
      controller: 'PeopleController as vm',
      templateUrl: 'people/people-list.html',
      activeMenu: 'people',
      resolve: {
        people: resolvePeople
      }
    });

    $stateProvider.state('app.view-person', {
      url: '/people/:id',
      controller: 'ViewPersonController as vm',
      templateUrl: 'people/view-person.html',
      activeMenu: 'person',
      resolve: {
        person: resolvePerson
      }
    });
  }

  angular.module('branchology.people', [
    'ui.router',
    'branchology.collections',
    'branchology.resources'
  ])
    .config(moduleConfig);

})();
