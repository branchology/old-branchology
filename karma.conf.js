module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['browserify', 'jasmine'],
    files: [
      'dist/*.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'src/**/*_test.js'
    ],
    autoWatch: true,
    browsers: ['PhantomJS'],
    preprocessors: {
      'test/*.js': ['browserify'],
      'src/**/*_test.js': ['browserify']
    },
    browserify: {
      debug: true
    },
    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-browserify'
    ]
  });
};
